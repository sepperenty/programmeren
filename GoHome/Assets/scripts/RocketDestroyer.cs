﻿using UnityEngine;
using System.Collections;

public class RocketDestroyer : MonoBehaviour
{

  public float ttl = 3f;
  public GameObject explosion;

  // Use this for initialization
  void Start()
  {

  }

  void OnTriggerEnter(Collider other)
  {
    if (other.tag != "rocket")
    {
      Destroy(gameObject);
      Instantiate(explosion, transform.position, transform.rotation);
    }
   
  }

  // Update is called once per frame
  void Update()
  {

    Destroy(gameObject, ttl);
    
  }
}
