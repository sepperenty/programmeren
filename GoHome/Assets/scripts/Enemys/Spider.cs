﻿using UnityEngine;
using System.Collections;

public class Spider {

  private Rigidbody spider;
  private Transform player;

  public Spider(Rigidbody initSpider, Transform initPlayer)
  {
    spider = initSpider;
    player = initPlayer;
  }

  public Rigidbody SpiderProp
  {
    get { return spider; }
    set { spider = value; }
  }

  public void move()
  {
    float distance = Vector3.Distance(spider.transform.position, player.transform.position);   //variabele die de afstand zal bekijken tussen Spider en Player
    Debug.Log(distance);
  }
	
}
