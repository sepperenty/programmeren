﻿using UnityEngine;
using System.Collections;

public class shoot : MonoBehaviour
{
  public GameObject gun;

  public GameObject bazooka;
  public Quaternion gunPosition;
  public Quaternion bazookaPosition;

  public bool gunSelected = true;
  public bool rocketSelected = false;
  public bool mineSelected = false;
  public Rigidbody rocketBullet;
  public int rocketTimer;
  public int rocketCanBeShot = 50;
  private int rocketCanNotBeShot = 0;
  public float rocketFirePower = 1000;
  private Bullet bullet = new Bullet(500, 5);

  //public float rocketSpeed = 1000f;
  public Transform endOfBarrelGun;
  public Transform endOfBarrelBazooka;






  // Use this for initialization
  void Start()
  {
    gun.SetActive(true);
    bazooka.SetActive(false);
    rocketTimer = 0;
  }

  // Update is called once per frame
  void Update()
  {
    WeaponCheck();

    //Rocket rocket = new Rocket(20f);

    if (Input.GetKey(KeyCode.Mouse0) && gunSelected)                                                                                 //Als de linker muistoets wordt in gedrukt.
    {
      Vector3 fwd = endOfBarrelGun.transform.TransformDirection(Vector3.forward);                                                   //zorgt ervoor dat de reycast steeds rechtdoor gaan van waar de GameObject naar richt
      bullet.shoot(endOfBarrelGun.transform.position, fwd * bullet.Distance);                                                        // de shoot methode van de Bullet wordt opgeroepen
    }

   

    if (Input.GetKey(KeyCode.Mouse0) && rocketSelected && rocketTimer == rocketCanBeShot)
    {
      rocketTimer = rocketCanNotBeShot;
      Rocket2 rocket2 = new Rocket2(5f, rocketBullet, rocketFirePower);
     
      rocket2.Rocket = Instantiate(rocketBullet, endOfBarrelBazooka.position, endOfBarrelBazooka.rotation) as Rigidbody;
      rocket2.moveRocket(endOfBarrelBazooka);
    }
    if (rocketTimer < rocketCanBeShot)
    {
      rocketTimer++;

    }

  }

  public void WeaponCheck()
  {
    if (Input.GetKey(KeyCode.F1))
    {
      gun.SetActive(true);

      bazooka.SetActive(false);

      gunSelected = true;
      rocketSelected = false;
      mineSelected = false;
    }

    else if (Input.GetKey(KeyCode.F2))
    {
      gun.SetActive(false);
      bazooka.SetActive(true);


      gunSelected = false;
      rocketSelected = true;
      mineSelected = false;
    }

    else if (Input.GetKey(KeyCode.F3))
    {
      gunSelected = false;
      rocketSelected = false;
      mineSelected = true;
    }



  }
}
