﻿using UnityEngine;
using System.Collections;

public class Projectile
{

  protected float damage = 15;
  protected Vector3 shootPower;


  public Projectile(float initDamage, Vector3 initShootpower)
  {

    damage = initDamage;
    shootPower = initShootpower;
  }

  public Projectile(float initDamage)
  {
    damage = initDamage;
  }

  public virtual void DamageWhenHit()
  { }

  public virtual void shoot(Rigidbody rocketClone)
  {
    rocketClone.AddForce(shootPower);

  }

  public virtual void makeExplosion()
  { }
}
