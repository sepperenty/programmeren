﻿using UnityEngine;
using System.Collections;

public class Rocket2
{

  protected Rigidbody rocket;
  protected float damage;
  protected float rocketPower;


  public Rocket2(float initDamage, Rigidbody initRocket, float initRocketPower)
  {
    rocket = initRocket;
    damage = initDamage;
    rocketPower = initRocketPower;
  }

  public Rigidbody Rocket
  {
    get { return rocket; }
    set { rocket = value; }
  }

  public void moveRocket(Transform endOfBarrel)
  {
    rocket.AddForce(endOfBarrel.transform.forward * rocketPower);
  }

  public void transfromToExplosion()
  { }

  public bool hitDetection()
  {
    if (rocket.detectCollisions)
    {
      return true;
    }

    else
      return false;
  }


}
