﻿using UnityEngine;
using System.Collections;

public class Bullet : Projectile
{

  private float bulletDistance;
  private float damage;

  public Bullet(float initBulletDistance, float initDamage)
    : base(initDamage)
  {
    bulletDistance = initBulletDistance;
    damage = initDamage;
  }

  public float Distance
  {
    get { return bulletDistance; }
  }

  //public override void shoot(Rigidbody rocketClone)
  //{
  //  base.shoot(rocketClone);
  //}

 

  public void shoot(Vector3 position, Vector3 forward)
  {
    RaycastHit hit;
    Ray shootingRay = new Ray(position, forward);
    Debug.DrawRay(position, forward);

    if (Physics.Raycast(shootingRay, out hit))
    {
      if (hit.collider.tag == "cube")
      {
        Debug.Log("Its a hit");
      }
    }
  }

}
