﻿using UnityEngine;
using System.Collections;

public class Rocket : Projectile
{

  private float shootPower = 600f;
  private float damage;
 
  private Rigidbody rocket;

  public Rocket(float initdamage)
    : base(initdamage)
  {

  }


  public void shootRocket(Transform endOfBarrel, Rigidbody rocketClone)
  {
    rocketClone.AddForce(endOfBarrel.transform.forward * shootPower);
  
  }

  


}
