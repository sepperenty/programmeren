﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

  public float movementSpeed = 30f;
  private Vector3 jump = new Vector3(0, 270, 0);
  private float maxSpeed = 5f;
  //public float jumpHeight;
  private Vector3 inputHorAndVer;
  //private Vector3 inputJump;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

    inputHorAndVer = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

    if (rigidbody.velocity.magnitude < maxSpeed)
    {
      rigidbody.AddForce(inputHorAndVer * movementSpeed);
    }
    
    if (Input.GetKeyDown("space"))
    {
      if (transform.position.y < 0.03)
      {
        rigidbody.AddForce(jump);
      }
    }
    
    
    

    //inputJump = new Vector3(0, Input.GetAxis("Jump"), 0);
    //rigidbody.AddForce(inputJump * jumpHeight);
    

  }
}
